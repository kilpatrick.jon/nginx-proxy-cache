const express = require('express');
const request = require('request');
const app = express();

const port = process.env.PORT || 3000;

app.use(express.json());
app.use(express.urlencoded({
    extended: true
}));

app.get("/hello-world", (req, res, next) => {
    res.json({
        "hello": "world"
    });
});

app.get("/proxy", (req, res, next) => {
    var options = {
        url: 'http://nginx/hello-world',
        method: 'GET',
        headers: {
            'XBackend': 'node:3000'
        }
    }
    request
        .get(options)
        .pipe(res)
});

app.listen(port, () => {
    console.log("Hello world, Running on port: " + port);
});